class GameView{

	constructor(){

	}

	afficheCoup(echiquier,socket){

	    socket.emit('update',echiquier);
	    socket.broadcast.emit('update',echiquier);

	}

	afficheBroadcastMessage(socket,message,sender =  "console"){
		socket.emit('chat', {text: message, sender: sender});
		socket.broadcast.emit('chat',{text: message, sender: sender});
	}

	afficheMessage(socket,message){
		socket.emit('chat',{text: message, sender: "console"});
	}
}

module.exports.gameView = function(){
	return new GameView();
}