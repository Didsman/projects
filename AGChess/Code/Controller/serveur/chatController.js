const gameC = require('./gameController.js');
const view = require('../../View/gameView.js');
const ent = require('ent');

class ChatController{

	constructor(){
		this.gameController = gameC.gameController(); 
		this.gameView = view.gameView();
		this.confirmation = true;
	}

	onMessage(socket,player,message){

		message = ent.encode(message);
	
		socket.confirmation = this.confirmation;
		//Trie des messages

		if(message.indexOf("@") == 0){//Si message normal

			let msg = player.status() + " : " + message.replace("@","");

			this.gameView.afficheBroadcastMessage(socket,msg,player.getCouleur());

		}else if(message.indexOf("/") == 0){			
			this.onCommande(socket,player,message.replace("/",""));
		}else{ // Si c'est un coup ou autre
		//trier

			if(socket.promotion)
				this.choixPromotionMessage(socket,player,message.toLowerCase());
			else if(socket.estValide)
				this.validationMessage(socket,player,message.toLowerCase());
			else
				this.gameController.onCoupChat(message,player,socket);	
		}
	}

	onCommande(socket,player,message){
		if(message.includes('help'))
			this.gameView.afficheMessage(socket, "Liste des commandes : \n /fen \n /moves \n /confirmation");
		else if(message.includes('fen')){
			this.gameView.afficheMessage(socket, "Chargement de la fen ...");
			message = message.replace("fen ","");
			this.gameController.onLoad(message,socket);
		}else if(message.includes('moves')){			
			this.gameView.afficheMessage(socket, "Liste des coups : " + this.gameController.partie.tousLesCoups());
			console.log(this.gameController.partie.tousLesCoups());
		}else if(message.includes('confirmation')){
			message = message.replace("confirmation ","");
			if(message.includes('on')){
				this.confirmation = true;
			}else if(message.includes('off')){
				this.confirmation = false
			}
			this.gameView.afficheMessage(socket, "La double confirmation est mise sur " +((this.confirmation)? 'on' : 'off'));
		}else
			this.gameView.afficheMessage(socket, "Cette commande n'existe pas ! Faite /help pour avoir la liste des commandes !");	
	}

	choixPromotionMessage(socket,player,message){
		if(message == 'b' || message == 'q' || message == 'n' || message == 'r'){
			socket.coup.promotion = message;
			this.gameController.onCoupPlateau(socket.coup,player,socket);
			this.gameController.onTourCourrant(socket);
		}else{
			this.gameView.afficheMessage(socket,"Selectionnez une piece ! (Q/B/N/R)");
		}
	}

	validationMessage(socket,player,message){
		if(message == "oui"){
			socket.validation = true;
			this.gameController.onCoupChat(message,player,socket);
		}else if(message == "non"){
			socket.estValide = false;	
		}else{
		this.gameView.afficheMessage(socket,"Veuillez confirmer votre coup en entrant 'oui' ou 'non'");
	}
}	

}

module.exports.chatController = function(){
	return new ChatController();
}