const game = require('../../Model/Game.js');
const view = require('../../View/gameView.js');

class GameController{

  constructor(){
    this.partie = game.game(true);
    this.gameView = view.gameView();
  }

  onCoupPlateau(coup,player,socket){

    if(this.partie.tourCourrant() == player.couleur){
        if(this.partie.estPromotion(coup) && !socket.promotion)
          this.onPromotion(socket,coup);
        else{
          if(this.partie.jouerCoupPlateau(coup) != null){

            this.resetAndUpdate(socket);//Envoit a la vue et reset les var

           }else{

            this.gameView.afficheMessage(socket,"Coup Invalide ! ");
         
           }
        }
     }else
      this.gameView.afficheMessage(socket,"Ce n'est pas votre Tour !");
  }

  onCoupChat(coup,player,socket){

    if(this.partie.tourCourrant() == player.couleur){

        if(!socket.estValide){ // Si le joueur n'a pas encore rentré de coup valide
          if(this.partie.verifCoup(coup)){ // Verifie si coup valide
            socket.estValide = true;
            socket.coup = coup;
            socket.validation = (!socket.confirmation);
            if(!socket.validation)
              this.gameView.afficheMessage(socket, "Vous voulez jouer "+coup+" ? (OUI/NON)");
          }else{
            this.gameView.afficheMessage(socket, "Coup Invalide ! \n Utilisez le caractère spécial '@' si vous voulez envoyer un message à votre adversaire !");
         }
        }

        if(socket.validation){ //Si le coup a été confirmé

          this.partie.jouerCoup(socket.coup); //joue le coup
          
          this.resetAndUpdate(socket);
          
      }
     }else{
      this.gameView.afficheMessage(socket, "Ce n'est pas votre Tour ! \n Utilisez le caractère spécial '@' si vous voulez envoyer un message à votre adversaire !")
     }
    
  }

   onPromotion(socket,coup){
    this.gameView.afficheMessage(socket,"Ce coup vous permet de promouvoir votre pion, Choisissez votre nouvelle pièce ! (Q/B/N/R)");         
    socket.estValide = true;
    socket.promotion = true;
    socket.coup = coup;
    }

  resetAndUpdate(socket){
    socket.coup="";
    socket.estValide = false;
    socket.validation = false;
    socket.promotion = false;

    if(this.partie.echec()){
      this.gameView.afficheBroadcastMessage(socket,"Echec !");
    }


    this.gameView.afficheCoup(this.partie.etat(),socket); //envoit a la vue
    this.onTourCourrant(socket);

    if(this.partie.end()) this.gameView.afficheBroadcastMessage(socket,this.partie.stringEnd(player));
  }

  onListeCoups(pos,socket,couleur){
    let c = (couleur == "white") ? 'w' : 'b';
    if(this.partie.tourCourrant() == c){
      socket.emit('listeCoups', this.partie.listeDesCoups(pos));
    }
  }

  onTourCourrant(socket){
    let tourCourrant = ((this.partie.tourCourrant() === 'w')? 'white' : 'black');
    if(this.partie.end())
      tourCourrant = null;
    socket.emit('tourCourrant', tourCourrant);
    socket.broadcast.emit('tourCourrant', tourCourrant);
  }

  onLoad(load,socket){

    if(this.partie.verifLoad(load)){
      this.gameView.afficheCoup(this.partie.etat(),socket);
    }else{
      this.gameView.afficheMessage(socket,"Les données de la partie sont érronées.");
      }
    }

  onUpdate(socket){
    this.gameView.afficheCoup(this.partie.etat(),socket);
  }

}

module.exports.gameController = function(){
  return new GameController();
}
