const Chess = require('../HTML/js/chess').Chess;
let chess = new Chess();

class Game{

    constructor(time){
      this.time = time;
    }

    verifCoup(coup){  // prend String coup et renvoie true ou false
    //Verifie le coup

     let x = false;
     chess.moves().forEach( i => {
          if (i == coup ) {
          x = true;
         }
     });
     return x;
    }

    estPromotion(coup){

      let x = false;
      let lCoups = this.listeDesCoups({square: coup.from, verbose: true});
      lCoups.forEach( (c) =>{
        x = x || c.promotion!=null;
      });
      return x;

    }

    jouerCoupPlateau(coup){
      return chess.move(coup);
    }

    listeDesCoups(pos){
      return chess.moves(pos);
    }

    tousLesCoups(){
      return chess.moves();
    }

    tourCourrant(){ //renvoit la couleur du joueur dont c'est la tour
        return chess.turn();
    }

    jouerCoup(coup){
      chess.move(coup);
    }

    etat(){
        return chess.fen();
    }

    end(){ //
      return chess.game_over();
    }

    echec(){
      return chess.in_check();
    }

    stringEnd(player){ //Renvoie String si le jeu s'est terminé via échec et mat, blocage, triple répétition ou manque de pièces.
       let M ;
      if (chess.in_checkmate()){
      	M = "Echec et mat ! Le joueur "+player.status()+" a gagné la partie !";
      }
      if(chess.in_stalemate()){
      	M ="Partie nulle ! Pat, le jeu est bloqué";
      }
      if(chess.insufficient_material()){
      	M ="Partie nulle ! Nombre de pièces insuffisant";
      }
      if(chess.in_threefold_repetition()){
      	M = " Partie nulle ! Triple répétition";
      }
      return M ;
    }

    verifLoad(load){
      return chess.load(load);
    }


}

module.exports.game = function(time){
    return new Game(time);
}
