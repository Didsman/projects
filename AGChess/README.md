# TER_L2_AGC

Projet_L2_ : Jeu d'échec pour malvoyant

Auteurs : CHEVALIER Baptiste, LEHAOUIAOUI Sara, LEMONNIER Adam, ZEDDAM Lylia, CAO ANH Lam

Sujet : Réaliser un jeu d'échec en ligne adapté à des utilisateur malvoyant 

Contenu du GIT:

[Documentation](../../tree/master/AGChess/Documentation): Présentation de l'architecture du site, cas d'utilisations et Screenshot du rendu.

[Code](../../tree/master/AGChess/Code): Code "pur" du site organisé en plusieurs sections. Controller pour la partie serveur, et View pour le côté client.

[Extras](../../tree/master/AGChess/Extras): Autres ressources utiles au projet (librairie chessboard.js, logos, photos, etc)

Le rapport est consultable et contient un lien vers une présentation vidéo du site et de ses caractéristiques.

*Une archive contenant notre code de Test est disponible (elle est presque identique au dossier "Code")*
