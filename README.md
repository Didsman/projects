# Projets

*Je m'appelle Adam Lemonnier, étudiant en informatique, je répertorie ici les deux projets de groupe que j'ai mené à bien dans le cadre de ma Licence (peut être quelques projets personels à venir)*

- [AGC](AGChess/): Projet Web réalisé en groupe de 5,  dans le cadre de l'UE Projets à la faculté des sciences durant l'année 2019/2020 (voir le README associé)

- [GAN](GAN/): Projet de DeepLearning réalisé en groupe de 4, dans le cadre de l'UE TER à la faculté des sciences durant l'année 2020/2021 (voir le README associé)
