# TER_L3_GAN
TER de L3 : GANs & modèles de génération de texte

Auteurs : CHEVALIER Baptiste, LEHAOUIAOUI Sara, LEMONNIER Adam, ZEDDAM Lylia 

Sujet : Etudier et comparer différentes architectures de GANs et différents modèles de génération de textes.

*Le rapport est consultable au format pdf ou via ce lien: https://fr.overleaf.com/read/tjgnrhgcqpjh*

Contenu du GIT:

- [Experimentation](../../tree/master/Experimentation.ipynb) : Expérimentation de l'efficacité de différents modèles de génération de textes (LSTM, Embeddings, GANs) sur différents jeux de données. Partie IV du rapport.

- [IrisGAN](../../tree/master/IrisGAN.ipynb) : Generation de vecteurs d'Iris artificiels depuis le jeu de données Iris en utilisant différenes architectures de GANs. Partie III du rapport.

- [LSTMGeneratingText](../../tree/master/LSTMForGeneratingText_VersioCommente.ipynb) : Modèles LSTM et Embeddings pour générer du texte.

- [LSTMGeneratingOpinion](../../tree/master/LSTMGeneratingOpinion.ipynb) : Modèle LSTM et Embeddings pour generer des textes exprimant des opinions.

Les fichiers utilisés dans les notebooks sont à extraire de l'archive Ressources.rar dans le dossier Ressources.
